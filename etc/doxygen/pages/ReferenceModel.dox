/** 
\page referencemodel %MMM Reference Model

The %MMM framework provides mechanisms to define reference models which can be adapted by \ref modelprocessor "Model Processors" in order to consider proband specific parameters.

\section mmm-model-kinematics Kinematic Specifications of the MMM Model
 
 This reference model has been created regarding the definitions used in Winter ("Biomechanics and motor control of human movement", David A. Winter).
 Below you can see the joint positions in the image below and their normalized positions in the table below regarding the body length L in horizontal and vertical directions.
 
 For example, given a model size of L=1,70m the size of the upper arm segment can be determined by multiplying the size L with the factor 0,188 (LS to LE) which results in a segment size of 31,96cm.
 
 The second table shows the names of the joints, and the decomposition of the acronyms, to give a better understanding, why those acronyms were chosen.

 
 
\image html MMM_kinematics.png "MMM model kinematic specification"



<br/>
<table border=1 cellpadding=0 cellspacing=0 style="border-collapse:collapse;table-layout:fixed;width:326pt">
 <tr>
  <td width=190 style='border-top:none;border-left:none;width:143pt'></td>
  <td width=113 style='width:100pt'><p><center>vertical direction</center></p></td>
  <td width=130 style='width:100pt'><p><center>horizontal direction</center></p></td>
 </tr>
 
 <tr>
  <td><b>Floor</b> to <b>RA/LA</b></td>
  <td><center>0,039</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>RA</b> to <b>RK, LA</b> to <b>LK</b></td>
  <td><center>0,246</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>RK</b> to <b>RH, LK</b> to <b>LH</b></td>
  <td><center>0,245</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>RH/LH</b> to <b>BP</b></td>
  <td><center>0,040</center></td>
  <td><center>0,052</center></td>
 </tr>
 <tr>
  <td><b>BP</b> to <b>BT</b></td>
  <td><center>0,060</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>BT</b> to <b>RSC/LSC</b></td>
  <td><center>0,188</center></td>
  <td><center>0,087</center></td>
 </tr>
 <tr>
  <td><b>RSC</b> to <b>RS, LSC</b> to <b>LS</b></td>
  <td><center>0,000</center></td>
  <td><center>0,023</center></td>
 </tr>
 <tr>
  <td><b>RS</b> to <b>RE, LS</b> to <b>LE</b></td>
  <td><center>0,188</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>RE</b> to <b>RW, LE</b> to <b>LW</b></td>
  <td><center>0.145</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>RW/LW</b> to <b>Fingertips</b></td>
  <td><center>0.108</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>BT</b> to <b>BLN</b></td>
  <td><center>0,210</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>BLN</b>  to <b>BUN</b></td>
  <td><center>0,030</center></td>
  <td><center>0,000</center></td>
 </tr>
 <tr>
  <td><b>BUN</b> to <b>Tip of Head</b></td>
  <td><center>0,130</center></td>
  <td><center>0,000</center></td>
 </tr>
</table>
<div class="caption">Normalized segment lengths regarding body height L.</div>


<br/>



<table border=1 cellpadding=0 cellspacing=0 style="border-collapse:collapse;table-layout:fixed;width:326pt">
 <tr>
  <td width=90 style='border-top:none;border-left:none;width:90pt'><p><center>joint name</center></p></td>
  <td width=236 style='width:236pt'><p><center>decomposition</center></p></td>
 </tr>
 
 <tr>
  <td><center><b>RA/LA</b></center></td>
  <td><center>Right/Left Ankle</center></td>
 </tr>
 <tr>
  <td><center><b>RK/LK</b></center></td>
  <td><center>Right/Left Knee</center></td>
 </tr>
 <tr>
  <td><center><b>RH/LH</b></center></td>
  <td><center>Right/Left Hip</center></td>
 </tr>
 <tr>
  <td><center><b>BP</b></center></td>
  <td><center>Body Pelvis</center></td>
 </tr>
 <tr>
  <td><center><b>BT</b></center></td>
  <td><center>Body Torso</center></td>
 </tr>
 <tr>
  <td><center><b>BLN</b></center></td>
  <td><center>Body Lower Neck</center></td>
 </tr>
 <tr>
  <td><center><b>BUN</b></center></td>
  <td><center>Body Upper Neck</center></td>
 </tr>
 <tr>
  <td><center><b>RSC/LSC</b></center></td>
  <td><center>Right/Left Shoulder Claviculum</center></td>
 </tr>
 <tr>
  <td><center><b>RS/LS</b></center></td>
  <td><center>Right/Left Shoulder</center></td>
 </tr>
 <tr>
  <td><center><b>RE/LE</b></center></td>
  <td><center>Right/Left Ellbow</center></td>
 </tr>
 <tr>
  <td><center><b>RW/LW</b></center></td>
  <td><span><center>Right/Left Wrist</center></span></td>
 </tr> 
</table>

\section mmm-model-dynamics Dynamic Specifications of the MMM Model
 Will be specified later.
 
 \section mmm-model-adaption Probad Specific Adaption of MMM Models

The MMM::Model may be processed by a \ref modelprocessor in order to adapt the kinematic and dynamic structure as well as the appearance to proband specific parameters.


*/
