/** 
\page mmmviewer MMMViewer

The MMMViewer is a graphical user interface to \ref mmmviewer_visualisation "visualise" and \ref mmmviewer_motionhandler "handle" motions in the \ref dataformat. For this purpose the MMMViewer is building on a \ref pluginarchitecture to easily extend the functionality without changing the underlying code.

Available basic functionality of the MMMViewer:
\li Open motions from the \ref dataformat
\li Save motions to the \ref dataformat
\li Play opened motions
\li Change number of frames per second of visualised motions
\li Change velocity of play speed
\li Enable antialiasing
\li Enable interpolation\n
<i>Standard:</i> Choose closest measurement on a specific timestep within a delta of 0,01s
\li Show visualisation table
\li Show floor grid
\li Load, activate/deactive all plugins at runtime

\section mmmviewer_visualisation Motion Visualisation
The visualization of the motions' individual sensors is based on specific corresponding sensor visualisation \ref pluginarchitecture "plugins".
These seperated visualisations can be toggled on and off. \n
As further advantage the visualisation of sensor measurements can be easily replaced by using a different implemented plugin.

Implementing a sensor visualisation plugin can be done by subclassing the header files <i>MMMTools/MMMViewer/SensorVisualisation/SensorVisualisation.h</i> and <i>MMMTools/MMMViewer/SensorVisualisation/SensorVisualisationFactory.h</i> similar to \ref plugin_creation "sensor plugins".\n
Current available examples can be found in the folder <i>MMMTools/MMMViewer/SensorVisualisation/SensorVisualisationPlugin</i>.

\section mmmviewer_motionhandler Motion Handler

Motion handler are various graphical user interface \ref pluginarchitecture "plugins" to extend the functionality of the MMMViewer in various ways.\n
Currently four different types are beeing considered:
\li <b>Import</b>\n
Import motions from different file formats (e.g. c3d) to the \ref dataformat
\li <b>Export</b>\n
Export motions from the \ref dataformat to various different file formats, videos, image sequences, ...
\li <b>Add Sensor</b>\n
Adding new sensors and sensor data to an existing motion in the \ref dataformat
\li <b>General</b>\n
General methods to handle motions in the \ref dataformat, like \ref converter_gui "converting", \ref segmentation_gui "segmenting", ...

The main idea behind using these plugins is to improve a user-friendly design of the %MMM framework by adding these user interface plugins to every project in MMMTools in addition to the available command line executables.

\subsection mmmviewer_creatingmotionhandler Creating a new Motion Handler

Implementing a new graphical user interface plugin for handling can be done by subclassing the header files <i>MMMTools/MMMViewer/MotionHandler.h</i> and <i>MMMTools/MMMViewer/MotionHandlerFactory.h</i>.\n
Current available examples can be found in various MMMTools projects in their respective folder <i>MMMTools/*/MotionHandlerPlugin</i>.\n
<b>To maintain the current style in the %MMM framework this extension should be done in a folder named <i>MotionHandlerPlugin</i> in the corresponding project's directory of MMMTools.</b>

Furthermore these gui plugins could also be used outside of the MMMViewer by implementing the specific independent motion handler interface (<i>MotionHandler</i> and <i>MotionHandlerFactory</i>) in your own project.

\section mmmviewer_plugins Plugins

The plugins for sensors, sensor visualisations, motion handler and specific plugins for motion handler can be loaded, activated and deactivated in the plugin ui window (Extra->Plugins) at runtime.
Furthermore the information about loaded, activated/deactivated plugins is stored locally and reused for upcoming sessions.
Changing the sensor plugins the motion needs to be reopened to apply changes to prevent an unintentional loss of work. 

\subsection mmmviwer_gui MMMViewer Graphical User Interface

The motion menu shows buttons for the execution of different motion handler plugins. The available MMMViewer functions (See \ref mmmviewer "top") should be self explanatory and do not need to be described in detail.

\image html MMMViewer.png "MMMViewer Graphical User Interface"

*/
