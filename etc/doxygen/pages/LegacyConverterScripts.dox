/** 
\page legacyconverterscripts [Legacy] %MMM Motion Convertation Scripts

This page will guide you through the process of converting an c3d-file into a legacy %MMM file and the new created legacy %MMM file into your desired robot xml file via automated scripts.

c3d-files can for example be found in the <a href="https://motion-database.humanoids.kit.edu/">KIT Whole-Body Human Motion Database</a>

Before executing the scripts MMMCore and MMMTools are needed to be built, see \ref installation.
Also the legacy motion tools need to be activated, see \ref legacytools.

\section c3dToMMM C3D to MMM

With the convert.sh script in MMMTools/ConverterVicon2MMM2Robot/MMM-scripts
you can convert your desired c3d files into MMM files. 
Adapt following things in the script:
- file paths

Another possibility is to convert everything in one step which is shown in the section “C3D to MMM to Robot”

You can convert your c3d file as follows:

./convert.sh #inputFile #outputFile

For Example:

./convert.sh 
MMMTools/ConverterVicon2MMM2Robot/ViconData/armmovement01.c3d MMMTools/ConverterVicon2MMM2Robot/MMMData/armmovement01.xml 
   
\section  mmmToRobot MMM to Robot

The given RobotData and Robot-scripts folders have to be adapted to your desired robot.
With the script convertMMM2Robot.sh in the folder Robot-scripts can be converted the MMM file for your desired robot.
Adapt following things:
- file paths
- robot files

You can convert your MMM file as follows:

./convertMMM2Robot.sh #inputFile #outputFile


For Example:
./convertMMM2Robot.sh MMMTools/ConverterVicon2MMM2Robot/MMMData/armmovement01.xml /MMMTools/ConverterVicon2MMM2Robot/RobotData/endmovement.xml 
 
\section c3dtoMMMtoRobot C3D to MMM to Robot

Another possibility is to convert everything in one step.
Therefore you can use the script automatic-convert.sh in the folder MMM-scripts

Adapt following things in automatic-convert.sh, convert.sh and convertMMM2Robot.sh:
- file paths
- robot files

This script enables you the possibility to convert several c3d files at once. Therefore, you have to store all your c3d files in the folder: MMMTools/ConverterVicon2MMM2Robot/ViconData

Then you have to just start the script to convert all files.

./automatic-convert.sh

*/

