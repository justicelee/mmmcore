/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#ifndef _MMM_MMMCore_h_
#define _MMM_MMMCore_h_


#ifdef WIN32
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

// needed to have M_PI etc defined
#if !defined(_USE_MATH_DEFINES)
#define _USE_MATH_DEFINES
#endif

#pragma warning(disable:4996) 

#endif


// allow std vector to be used with Eigen objects

#include <Eigen/StdVector>
#ifndef EIGEN_STL_VECTOR_SPECIFICATION_DEFINED
#define EIGEN_STL_VECTOR_SPECIFICATION_DEFINED
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector2f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector3f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector4f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::VectorXf)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix2f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix3f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Matrix4f)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::MatrixXf)

EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector3d)
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector3i)
#endif 
#include <boost/shared_ptr.hpp>
#include <boost/assert.hpp>
#include <boost/weak_ptr.hpp>
#include <iostream>
#include <sstream>
#include <cmath>

#include "MMMImportExport.h"

/*!
	\brief The main MMM namespace.
*/
namespace MMM
{
    // only valid within the MMM namespace
    using std::cout;
    using std::endl;

#define MMM_INFO std::cout <<__FILE__ << ":" << __LINE__ << ": "
#define MMM_WARNING std::cerr <<__FILE__ << ":" << __LINE__ << " -Warning- "
#define MMM_ERROR std::cerr <<__FILE__ << ":" << __LINE__ << " - ERROR - "

	
#ifdef _DEBUG
/*!
	This assert macro does nothing on RELEASE builds.
*/
#define VR_ASSERT( a )  BOOST_ASSERT( a )
	//THROW_MMM_EXCEPTION_IF(!(a), "ASSERT failed (" << #a << ")" );

	// we have to switch to boost 1.48 to allow messages (BOOST_ASSERT_MSG) ....
#define MMM_ASSERT_MESSAGE(a,b) BOOST_ASSERT(a)
	//THROW_MMM_EXCEPTION_IF(!(a), "ASSERT failed (" << #a << "): " << b );
#else
#define MMM_ASSERT(a)
#define MMM_ASSERT_MESSAGE(a,b)
#endif



} // namespace

#endif // _MMM_MMMCORE_h_
