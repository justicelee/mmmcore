/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_EXCEPTIONS_H_
#define __MMM_EXCEPTIONS_H_

#include <string>

namespace MMM
{
    namespace Exception
    {
        class MMMException : public std::exception
        {
          protected:
            std::string err_msg;

          public:
            MMMException(const std::string &msg = "MMMCoreException thrown") : err_msg(msg)  {}

            ~MMMException() throw() {}

            const char *what() const throw() { return this->err_msg.c_str(); }
        };

        class XMLFormatException : public MMMException
        {
          public:
            XMLFormatException(const std::string &msg = "XMLFormatException thrown") : MMMException(msg)  {}

            ~XMLFormatException() throw() {}
        };

        class ForcedCancelException : public std::exception
        {
          public:
            ForcedCancelException() {}

            ~ForcedCancelException() throw() {}
        };

    }
}

#endif // __MMM_EXCEPTIONS_H_
