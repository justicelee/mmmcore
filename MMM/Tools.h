/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_TOOLS_H_
#define __MMM_TOOLS_H_

#include <algorithm>
#include <map>

namespace MMM {
    namespace Tools {

        template <typename Map>
        bool compare(Map const &m1, Map const &m2) {
            auto pred = [] (decltype(*m1.begin()) a, decltype(a) b)
                               { return a.first == b.first && a.second == b.second; };

                return m1.size() == m2.size()
                    && std::equal(m1.begin(), m1.end(), m2.begin(), pred);
        }

        template <typename Key, typename Value>
        std::vector<Key> getKeysFromMap(const std::map<Key, Value> &map) {
            std::vector<Key> keys;
            for (auto const &m : map) keys.push_back(m);
            return keys;
        }

    }
}

#endif // _MMM_TOOLS_H_
