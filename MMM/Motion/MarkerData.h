/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MarkerData_H_
#define __MMM_MarkerData_H_

#include "../MMMCore.h"
#include <string>
#include <map>
#include <Eigen/Core>
#include <fstream>
#include <iostream>

#include "../MMMImportExport.h"


namespace MMM
{

/*! \brief Stores three-dimensional position of unlabeled and labeled motion capture markers. */
class MMM_IMPORT_EXPORT MarkerData
{
public:
    MarkerData();

    MarkerData(const std::map<std::string, Eigen::Vector3f> &labeledData, const std::vector<Eigen::Vector3f> &unlabeledData);

    bool hasLabeledData();

    bool hasLabel(const std::string &label);

    Eigen::Vector3f getData(const std::string &label);

    std::map<std::string, Eigen::Vector3f> getLabeledData();

    std::vector<Eigen::Vector3f> getUnlabeledData();

    bool addMarkerData(const Eigen::Vector3f &data, const std::string &label = std::string());

private:
    /*! Labeled marker position data. String labels of motion capture marker mapped onto their position. */
    std::map<std::string, Eigen::Vector3f> labeledData;

    /*! Unlabeled motion capture marker position data. */
    std::vector<Eigen::Vector3f> unlabeledData;
};
typedef boost::shared_ptr<MarkerData> MarkerDataPtr;

}

#endif
