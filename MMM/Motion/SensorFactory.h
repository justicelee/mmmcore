/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORFACTORY_H_
#define __MMM_SENSORFACTORY_H_

#include <boost/shared_ptr.hpp>
#include <boost/extension/extension.hpp>
#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>

#include "Sensor.h"

namespace MMM
{

class MMM_IMPORT_EXPORT SensorFactory : public AbstractFactoryMethod<SensorFactory, void*>
{
public:
    SensorFactory() { }

    virtual ~SensorFactory() { }

    /*! Creates a sensor from an xml representation.
        @param sensorNode The sensor xml node. */
    virtual SensorPtr createSensor(RapidXMLReaderNodePtr sensorNode) = 0;

    /*! Returns the sensor type as name of the factory. */
    virtual std::string getName() = 0;

    static constexpr const char* VERSION = "1.0";
};

typedef boost::shared_ptr<SensorFactory> SensorFactoryPtr;

template<class Sensor>
std::string NAME_STR(){
    return std::string(Sensor::TYPE) + "_v" + std::string(Sensor::VERSION);
}

}

#endif
