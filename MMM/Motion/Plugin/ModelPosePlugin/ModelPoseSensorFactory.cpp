#include "ModelPoseSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "ModelPoseSensor.h"

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry ModelPoseSensorFactory::registry(NAME_STR<ModelPoseSensor>(), &ModelPoseSensorFactory::createInstance);

ModelPoseSensorFactory::ModelPoseSensorFactory() : SensorFactory() {}

ModelPoseSensorFactory::~ModelPoseSensorFactory() {}

SensorPtr ModelPoseSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return ModelPoseSensor::loadSensorXML(sensorNode);
}

std::string ModelPoseSensorFactory::getName()
{
    return NAME_STR<ModelPoseSensor>();
}

SensorFactoryPtr ModelPoseSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new ModelPoseSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new ModelPoseSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
