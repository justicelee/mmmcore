project(ModelPoseSensor)

set(ModelPoseSensor_Sources
    ModelPoseSensor.cpp
    ModelPoseSensorFactory.cpp
    ModelPoseSensorMeasurement.cpp
)

set(ModelPoseSensor_Headers
    ModelPoseSensor.h
    ModelPoseSensorFactory.h
    ModelPoseSensorMeasurement.h
)

DefineMotionSensorPlugin(${PROJECT_NAME} "${ModelPoseSensor_Sources}" "${ModelPoseSensor_Headers}")
