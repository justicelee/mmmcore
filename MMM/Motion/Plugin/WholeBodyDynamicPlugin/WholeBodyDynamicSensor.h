/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_WHOLEBODYDYNAMICSENSOR_H_
#define __MMM_WHOLEBODYDYNAMICSENSOR_H_

#include "../../InterpolatableSensor.h"
#include "../../SensorMeasurement.h"
#include "../../../RapidXML/RapidXMLReader.h"
#include "../../../RapidXML/RapidXMLWriter.h"
#include "WholeBodyDynamicSensorMeasurement.h"

#include <vector>

namespace MMM
{

class WholeBodyDynamicSensor;

typedef boost::shared_ptr<WholeBodyDynamicSensor> WholeBodyDynamicSensorPtr;
typedef std::vector<WholeBodyDynamicSensorPtr> WholeBodyDynamicSensorList;

class WholeBodyDynamicSensor : public InterpolatableSensor<WholeBodyDynamicSensorMeasurement>
{

public:
    static WholeBodyDynamicSensorPtr loadSensorXML(RapidXMLReaderNodePtr node);

    WholeBodyDynamicSensor(const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    boost::shared_ptr<BasicSensor<WholeBodyDynamicSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "WholeBodyDynamic";

    static constexpr const char* VERSION = "1.0";

private:
    void loadConfigurationXML(RapidXMLReaderNodePtr node);

    void loadMeasurementXML(RapidXMLReaderNodePtr node);

    void appendConfigurationXML(RapidXMLWriterNodePtr node);
};


}
#endif // __MMM_WholeBodyDynamicSENSOR_H_

