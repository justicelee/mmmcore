/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_WHOLEBODYDYNAMICSENSORMEASUREMENT_H_
#define __MMM_WHOLEBODYDYNAMICSENSORMEASUREMENT_H_

#include "../../SensorMeasurement.h"
#include "../../Interpolate.h"

#include <Eigen/Core>

namespace MMM
{
class WholeBodyDynamicSensorMeasurement;

typedef boost::shared_ptr<WholeBodyDynamicSensorMeasurement> WholeBodyDynamicSensorMeasurementPtr;

class WholeBodyDynamicSensorMeasurement : public SensorMeasurement, Interpolate<WholeBodyDynamicSensorMeasurement>, SMCloneable<WholeBodyDynamicSensorMeasurement>
{

public:
    WholeBodyDynamicSensorMeasurement(float timestep, const Eigen::Vector3f &centerOfMass, const Eigen::Vector3f &angularMomentum, bool interpolated = false);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    WholeBodyDynamicSensorMeasurementPtr clone(float newTimestep);

    WholeBodyDynamicSensorMeasurementPtr interpolate(WholeBodyDynamicSensorMeasurementPtr other, float timestep);

    Eigen::Vector3f getCenterOfMass();

    Eigen::Vector3f getAngularMomentum();

protected:
    void appendMeasurementDataXML(RapidXMLWriterNodePtr measurementNode);

private:
    Eigen::Vector3f centerOfMass;
    Eigen::Vector3f angularMomentum;
};
}
#endif // __MMM_WholeBodyDynamicSENSORMEASUREMENT_H_
