project(WholeBodyDynamicSensor)

set(WholeBodyDynamicSensor_Sources
    WholeBodyDynamicSensor.cpp
    WholeBodyDynamicSensorFactory.cpp
    WholeBodyDynamicSensorMeasurement.cpp
)

set(WholeBodyDynamicSensor_Headers
    WholeBodyDynamicSensor.h
    WholeBodyDynamicSensorFactory.h
    WholeBodyDynamicSensorMeasurement.h
)

DefineMotionSensorPlugin(${PROJECT_NAME} "${WholeBodyDynamicSensor_Sources}" "${WholeBodyDynamicSensor_Headers}")
