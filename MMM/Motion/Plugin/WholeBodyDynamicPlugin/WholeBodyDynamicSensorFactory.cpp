#include "WholeBodyDynamicSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "WholeBodyDynamicSensor.h"

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry WholeBodyDynamicSensorFactory::registry(NAME_STR<WholeBodyDynamicSensor>(), &WholeBodyDynamicSensorFactory::createInstance);

WholeBodyDynamicSensorFactory::WholeBodyDynamicSensorFactory() : SensorFactory() {}

WholeBodyDynamicSensorFactory::~WholeBodyDynamicSensorFactory() {}

SensorPtr WholeBodyDynamicSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return WholeBodyDynamicSensor::loadSensorXML(sensorNode);
}

std::string WholeBodyDynamicSensorFactory::getName()
{
    return NAME_STR<WholeBodyDynamicSensor>();
}

SensorFactoryPtr WholeBodyDynamicSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new WholeBodyDynamicSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new WholeBodyDynamicSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
