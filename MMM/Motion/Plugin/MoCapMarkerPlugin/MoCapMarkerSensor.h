/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOCAPMARKERSENSOR_H_
#define __MMM_MOCAPMARKERSENSOR_H_

#include "../../InterpolatableSensor.h"
#include "../../SensorMeasurement.h"
#include "../../../RapidXML/RapidXMLReader.h"
#include "../../../RapidXML/RapidXMLWriter.h"
#include "MoCapMarkerSensorMeasurement.h"

#include <vector>

namespace MMM
{

class MoCapMarkerSensor;

typedef boost::shared_ptr<MoCapMarkerSensor> MoCapMarkerSensorPtr;
typedef std::vector<MoCapMarkerSensorPtr> MoCapMarkerSensorList;

class MoCapMarkerSensor : public InterpolatableSensor<MoCapMarkerSensorMeasurement>
{

public:

    static MoCapMarkerSensorPtr loadSensorXML(RapidXMLReaderNodePtr node);

    MoCapMarkerSensor(const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    boost::shared_ptr<BasicSensor<MoCapMarkerSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    std::map<float, std::map<std::string, Eigen::Vector3f>> getLabeledMarkerData();

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "MoCapMarker";

    static constexpr const char* VERSION = "1.0";

private:

    void loadConfigurationXML(RapidXMLReaderNodePtr node);

    void loadMeasurementXML(RapidXMLReaderNodePtr node);

    void appendConfigurationXML(RapidXMLWriterNodePtr node);

};


}
#endif // __MMM_MoCapMarkerSENSOR_H_

