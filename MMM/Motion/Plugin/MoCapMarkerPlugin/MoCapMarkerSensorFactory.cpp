#include "MoCapMarkerSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "MoCapMarkerSensor.h"

#include <cstring>

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry MoCapMarkerSensorFactory::registry(NAME_STR<MoCapMarkerSensor>(), &MoCapMarkerSensorFactory::createInstance);

MoCapMarkerSensorFactory::MoCapMarkerSensorFactory() : SensorFactory() {}

MoCapMarkerSensorFactory::~MoCapMarkerSensorFactory() {}

SensorPtr MoCapMarkerSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return MoCapMarkerSensor::loadSensorXML(sensorNode);
}

std::string MoCapMarkerSensorFactory::getName()
{
    return NAME_STR<MoCapMarkerSensor>();
}

SensorFactoryPtr MoCapMarkerSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new MoCapMarkerSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new MoCapMarkerSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
