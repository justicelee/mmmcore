#include "KinematicSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "KinematicSensor.h"

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry KinematicSensorFactory::registry(NAME_STR<KinematicSensor>(), &KinematicSensorFactory::createInstance);

KinematicSensorFactory::KinematicSensorFactory() : SensorFactory() {}

KinematicSensorFactory::~KinematicSensorFactory() {}

SensorPtr KinematicSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return KinematicSensor::loadSensorXML(sensorNode);
}

std::string KinematicSensorFactory::getName()
{
    return NAME_STR<KinematicSensor>();
}

SensorFactoryPtr KinematicSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new KinematicSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new KinematicSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
