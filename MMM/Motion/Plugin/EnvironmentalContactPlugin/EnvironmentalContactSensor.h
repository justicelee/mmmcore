/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ENVIRONMENTALCONTACTSENSOR_H_
#define __MMM_ENVIRONMENTALCONTACTSENSOR_H_

#include "../../BasicSensor.h"
#include "../../SensorMeasurement.h"
#include "../../../RapidXML/RapidXMLReader.h"
#include "../../../RapidXML/RapidXMLWriter.h"
#include "EnvironmentalContactSensorMeasurement.h"

#include <vector>

namespace MMM
{

class EnvironmentalContactSensor;

typedef boost::shared_ptr<EnvironmentalContactSensor> EnvironmentalContactSensorPtr;
typedef std::vector<EnvironmentalContactSensorPtr> EnvironmentalContactSensorList;

class EnvironmentalContactSensor : public BasicSensor<EnvironmentalContactSensorMeasurement>
{

public:
    static EnvironmentalContactSensorPtr loadSensorXML(RapidXMLReaderNodePtr node);

    EnvironmentalContactSensor(const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    boost::shared_ptr<BasicSensor<EnvironmentalContactSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    EnvironmentalContactSensorMeasurementPtr getDerivedMeasurement(float timestep);

    EnvironmentalContactSensorMeasurementPtr getDerivedMeasurement(float timestep, float delta);

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "EnvironmentalContact";

    static constexpr const char* VERSION = "1.0";

private:
    void loadConfigurationXML(RapidXMLReaderNodePtr node);

    void loadMeasurementXML(RapidXMLReaderNodePtr node);

    void appendConfigurationXML(RapidXMLWriterNodePtr node);
};


}
#endif // __MMM_ENVIRONMENTALCONTACTSENSOR_H_

