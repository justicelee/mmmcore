#include "EnvironmentalContactSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "EnvironmentalContactSensor.h"

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry EnvironmentalContactSensorFactory::registry(NAME_STR<EnvironmentalContactSensor>(), &EnvironmentalContactSensorFactory::createInstance);

EnvironmentalContactSensorFactory::EnvironmentalContactSensorFactory() : SensorFactory() {}

EnvironmentalContactSensorFactory::~EnvironmentalContactSensorFactory() {}

SensorPtr EnvironmentalContactSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return EnvironmentalContactSensor::loadSensorXML(sensorNode);
}

std::string EnvironmentalContactSensorFactory::getName()
{
    return NAME_STR<EnvironmentalContactSensor>();
}

SensorFactoryPtr EnvironmentalContactSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new EnvironmentalContactSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new EnvironmentalContactSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
