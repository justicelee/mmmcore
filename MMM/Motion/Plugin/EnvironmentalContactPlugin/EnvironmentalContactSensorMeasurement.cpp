#include "../../../XMLTools.h"
#include "../../../MathTools.h"
#include "../../../Tools.h"

#include "EnvironmentalContactSensorMeasurement.h"

using namespace MMM;

EnvironmentalContactSensorMeasurement::EnvironmentalContactSensorMeasurement(float timestep, const std::map<std::string, std::string> &EnvironmentalContact, bool interpolated) :
    SensorMeasurement(timestep, interpolated),
    EnvironmentalContact(EnvironmentalContact)
{
}

SensorMeasurementPtr EnvironmentalContactSensorMeasurement::clone() {
    return clone(timestep);
}

bool EnvironmentalContactSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    EnvironmentalContactSensorMeasurementPtr ptr = boost::dynamic_pointer_cast<EnvironmentalContactSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return Tools::compare<std::map<std::string, std::string> >(EnvironmentalContact, ptr->EnvironmentalContact);
    }
    return false;
}

EnvironmentalContactSensorMeasurementPtr EnvironmentalContactSensorMeasurement::clone(float newTimestep) {
    EnvironmentalContactSensorMeasurementPtr clonedSensorMeasurement(new EnvironmentalContactSensorMeasurement(newTimestep, EnvironmentalContact, interpolated));
    return clonedSensorMeasurement;
}

void EnvironmentalContactSensorMeasurement::appendMeasurementDataXML(RapidXMLWriterNodePtr measurementNode) {
    for (const auto &contact : EnvironmentalContact) {
        measurementNode->append_node("Contact")->append_attribute("segment", contact.first)->append_data_node(contact.second);
    }
}

std::map<std::string, std::string> EnvironmentalContactSensorMeasurement::getEnvironmentalContact() {
    return EnvironmentalContact;
}
