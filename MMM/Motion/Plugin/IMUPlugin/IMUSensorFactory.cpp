#include "IMUSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "IMUSensor.h"

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry IMUSensorFactory::registry(NAME_STR<IMUSensor>(), &IMUSensorFactory::createInstance);

IMUSensorFactory::IMUSensorFactory() : SensorFactory() {}

IMUSensorFactory::~IMUSensorFactory() {}

SensorPtr IMUSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return IMUSensor::loadSensorXML(sensorNode);
}

std::string IMUSensorFactory::getName()
{
    return NAME_STR<IMUSensor>();
}

SensorFactoryPtr IMUSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new IMUSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new IMUSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
