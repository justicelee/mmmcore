/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_IMUSENSOR_H_
#define __MMM_IMUSENSOR_H_

#include "../../InterpolatableSensor.h"
#include "../../SensorMeasurement.h"
#include "../../../RapidXML/RapidXMLReader.h"
#include "../../../RapidXML/RapidXMLWriter.h"
#include "IMUSensorMeasurement.h"

#include <Eigen/Core>
#include <vector>

namespace MMM
{

class IMUSensor;

typedef boost::shared_ptr<IMUSensor> IMUSensorPtr;
typedef std::vector<IMUSensorPtr> IMUSensorList;

class IMUSensor : public InterpolatableSensor<IMUSensorMeasurement>
{

public:
    static IMUSensorPtr loadSensorXML(RapidXMLReaderNodePtr node);

    static IMUSensorPtr loadSensorConfigurationXML(RapidXMLReaderNodePtr configurationNode);

    IMUSensor(const std::string &segment, const std::string &offsetUnit = "mm", const Eigen::Matrix4f &offset = Eigen::Matrix4f::Identity(), const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    boost::shared_ptr<BasicSensor<IMUSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    std::string getType();

    std::string getVersion();

    int getPriority();

    std::string getSegment();

    std::string getOffsetUnit();

    Eigen::Matrix4f getOffset();

    static constexpr const char* TYPE = "IMU";

    static constexpr const char* VERSION = "1.0";

private:
    IMUSensor();

    void loadConfigurationXML(RapidXMLReaderNodePtr node);

    void loadMeasurementXML(RapidXMLReaderNodePtr node);

    void appendConfigurationXML(RapidXMLWriterNodePtr node);

    std::string segment;

    std::string offsetUnit;

    Eigen::Matrix4f offset;
};


}
#endif // __MMM_IMUSENSOR_H_

