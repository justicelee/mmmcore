#include <cassert>
#include <boost/algorithm/string.hpp>
#include <map>

#include "../../../XMLTools.h"

#include "IMUSensor.h"

using namespace MMM;

IMUSensorPtr IMUSensor::loadSensorXML(RapidXMLReaderNodePtr node) {
    IMUSensorPtr sensor(new IMUSensor());
    sensor->loadSensor(node);
    return sensor;
}

IMUSensorPtr IMUSensor::loadSensorConfigurationXML(RapidXMLReaderNodePtr configurationNode) {
    IMUSensorPtr sensor(new IMUSensor());
    sensor->loadConfigurationXML(configurationNode->first_node("Configuration"));
    return sensor;
}

IMUSensor::IMUSensor() : InterpolatableSensor()
{
}

IMUSensor::IMUSensor(const std::string &segment, const std::string &offsetUnit, const Eigen::Matrix4f &offset, const std::string &description) :
    InterpolatableSensor(description),
    segment(segment),
    offsetUnit(offsetUnit),
    offset(offset)
{
}

bool IMUSensor::checkModel(ModelPtr model) {
    if (!model) {
        MMM_ERROR << "Sensor '" << uniqueName << "' needs a model."  << std::endl;
        return false;
    }
    if (model->getModelNode(segment)) return true;
    else return false;
}

boost::shared_ptr<BasicSensor<IMUSensorMeasurement> > IMUSensor::cloneConfiguration() {
    IMUSensorPtr m(new IMUSensor(segment, offsetUnit, offset, description));
    return m;
}

void IMUSensor::loadConfigurationXML(RapidXMLReaderNodePtr node) {
    assert(node);
    assert(node->name() == "Configuration");

    segment = node->first_node("Segment")->attribute_value("name");
    if (node->has_node("Offset")) {
        RapidXMLReaderNodePtr offsetNode = node->first_node("Offset");
        offsetUnit = offsetNode->attribute_value("unit");
        offset = XML::process4x4Matrix(offsetNode->first_node("Matrix4x4"));
    } else {
        offsetUnit = "mm";
        offset = Eigen::Matrix4f::Identity();
    }
}

void IMUSensor::loadMeasurementXML(RapidXMLReaderNodePtr node)
{
    assert(node);
    assert(node->name() == XML::NODE_MEASUREMENT);

    RapidXMLReaderNodePtr accNode = node->first_node("Acceleration");
    IMUSensorMeasurementPtr m(new IMUSensorMeasurement(XML::readFloat(node->attribute_value(XML::ATTRIBUTE_TIMESTEP).c_str()), XML::readFloatArray(accNode->value(), 3)));
    addSensorMeasurement(m);
}

void IMUSensor::appendConfigurationXML(RapidXMLWriterNodePtr node)
{
    assert(node);

    RapidXMLWriterNodePtr configurationNode = node->append_node("Configuration");
    configurationNode->append_node("Segment")->append_attribute("name", segment);
    RapidXMLWriterNodePtr offsetNode = configurationNode->append_node("Offset");
    offsetNode->append_attribute("unit", offsetUnit);
    XML::appendXML(offset, offsetNode, "Matrix4x4");
}

bool IMUSensor::equalsConfiguration(SensorPtr other) {
    IMUSensorPtr ptr = boost::dynamic_pointer_cast<IMUSensor>(other);
    if (ptr) {
        return segment == ptr->segment && offset == ptr->offset && offsetUnit == ptr->offsetUnit;
    }
    else return false;
}

std::string IMUSensor::getType() {
    return TYPE;
}

std::string IMUSensor::getVersion() {
    return VERSION;
}

int IMUSensor::getPriority() {
    return 70;
}

std::string IMUSensor::getSegment() {
    return segment;
}

std::string IMUSensor::getOffsetUnit() {
    return offsetUnit;
}

Eigen::Matrix4f IMUSensor::getOffset() {
    return offset;
}
