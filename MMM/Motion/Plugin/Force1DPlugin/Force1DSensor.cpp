#include <cassert>
#include <boost/algorithm/string.hpp>
#include <map>

#include "../../../XMLTools.h"

#include "Force1DSensor.h"

using namespace MMM;

Force1DSensorPtr Force1DSensor::loadSensorXML(RapidXMLReaderNodePtr node) {
    Force1DSensorPtr sensor(new Force1DSensor());
    sensor->loadSensor(node);
    return sensor;
}

Force1DSensor::Force1DSensor() : InterpolatableSensor()
{
}

Force1DSensor::Force1DSensor(const std::string &segment, const std::string &offsetUnit, const Eigen::Matrix4f &offset, const std::string &description) :
    InterpolatableSensor(description),
    segment(segment),
    offsetUnit(offsetUnit),
    offset(offset)
{
}

bool Force1DSensor::checkModel(ModelPtr model) {
    if (!model) {
        MMM_ERROR << "Sensor '" << uniqueName << "' needs a model."  << std::endl;
        return false;
    }
    if (model->getModelNode(segment)) return true;
    else return false;
}

boost::shared_ptr<BasicSensor<Force1DSensorMeasurement> > Force1DSensor::cloneConfiguration() {
    Force1DSensorPtr m(new Force1DSensor(segment, offsetUnit, offset, description));
    return m;
}

void Force1DSensor::loadConfigurationXML(RapidXMLReaderNodePtr node) {
    assert(node);
    assert(node->name() == "Configuration");

    segment = node->first_node("Segment")->attribute_value("name");
    RapidXMLReaderNodePtr offsetNode = node->first_node("Offset");
    offsetUnit = offsetNode->attribute_value("unit");
    offset = XML::process4x4Matrix(offsetNode->first_node("Matrix4x4"));
}

void Force1DSensor::loadMeasurementXML(RapidXMLReaderNodePtr node)
{
    assert(node);
    assert(node->name() == XML::NODE_MEASUREMENT);

    RapidXMLReaderNodePtr forceNode = node->first_node("Force");
    Force1DSensorMeasurementPtr m(new Force1DSensorMeasurement(XML::readFloat(node->attribute_value(XML::ATTRIBUTE_TIMESTEP).c_str()), XML::readFloat((forceNode->value()).c_str())));
    addSensorMeasurement(m);
}

void Force1DSensor::appendConfigurationXML(RapidXMLWriterNodePtr node)
{
    assert(node);

    RapidXMLWriterNodePtr configurationNode = node->append_node("Configuration");
    configurationNode->append_node("Segment")->append_attribute("name", segment);
    RapidXMLWriterNodePtr offsetNode = configurationNode->append_node("Offset");
    offsetNode->append_attribute("unit", offsetUnit);
    XML::appendXML(offset, offsetNode, "Matrix4x4");
}

bool Force1DSensor::equalsConfiguration(SensorPtr other) {
    Force1DSensorPtr ptr = boost::dynamic_pointer_cast<Force1DSensor>(other);
    if (ptr) {
        return segment == ptr->segment && offset == ptr->offset && offsetUnit == ptr->offsetUnit;
    }
    else return false;
}

std::string Force1DSensor::getType() {
    return TYPE;
}

std::string Force1DSensor::getVersion() {
    return VERSION;
}

int Force1DSensor::getPriority() {
    return 60;
}

std::string Force1DSensor::getSegment() {
    return segment;
}

std::string Force1DSensor::getOffsetUnit() {
    return offsetUnit;
}

Eigen::Matrix4f Force1DSensor::getOffset() {
    return offset;
}
