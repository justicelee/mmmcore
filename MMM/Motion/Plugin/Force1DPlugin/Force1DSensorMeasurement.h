/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_FORCE1DSENSORMEASUREMENT_H_
#define __MMM_FORCE1DSENSORMEASUREMENT_H_

#include "../../SensorMeasurement.h"
#include "../../Interpolate.h"

#include <Eigen/Core>

namespace MMM
{
class Force1DSensorMeasurement;

typedef boost::shared_ptr<Force1DSensorMeasurement> Force1DSensorMeasurementPtr;

class Force1DSensorMeasurement : public SensorMeasurement, Interpolate<Force1DSensorMeasurement>, SMCloneable<Force1DSensorMeasurement>
{

public:
    Force1DSensorMeasurement(float timestep, float force, bool interpolated = false);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    Force1DSensorMeasurementPtr clone(float newTimestep);

    Force1DSensorMeasurementPtr interpolate(Force1DSensorMeasurementPtr other, float timestep);

    float getForce();

protected:
    void appendMeasurementDataXML(RapidXMLWriterNodePtr measurementNode);

private:
    float force;
};
}
#endif // __MMM_Force1DSENSORMEASUREMENT_H_
