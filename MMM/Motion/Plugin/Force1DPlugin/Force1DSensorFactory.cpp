#include "Force1DSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "Force1DSensor.h"

using namespace MMM;

// register this factory
SensorFactory::SubClassRegistry Force1DSensorFactory::registry(NAME_STR<Force1DSensor>(), &Force1DSensorFactory::createInstance);

Force1DSensorFactory::Force1DSensorFactory() : SensorFactory() {}

Force1DSensorFactory::~Force1DSensorFactory() {}

SensorPtr Force1DSensorFactory::createSensor(RapidXMLReaderNodePtr sensorNode)
{
    return Force1DSensor::loadSensorXML(sensorNode);
}

std::string Force1DSensorFactory::getName()
{
    return NAME_STR<Force1DSensor>();
}

SensorFactoryPtr Force1DSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new Force1DSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new Force1DSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}
