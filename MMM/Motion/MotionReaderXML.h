/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONREADERXML_H_
#define __MMM_MOTIONREADERXML_H_

#include "../MMMCore.h"
#include "../MMMImportExport.h"
#include "../RapidXML/RapidXMLReader.h"
#include "../FactoryPluginLoader.h"
#include "Motion.h"
#include "SensorFactory.h"

#include <string>
#include <set>

namespace MMM
{
/*! \brief A generic xml reader for motions in the mmm dataformat */
class MMM_IMPORT_EXPORT MotionReaderXML
{

public:
    /*! @param additionalLibPaths Paths to sensor plug-in folders. No need to include the standard folder.
        @param ignoreStandardLibPaths Just use the given additional library Paths. */
    MotionReaderXML(const std::vector<std::string> &additionalLibPaths = std::vector<std::string>(), bool ignoreStandardLibPaths = false);

    /*! @param sensorFactories The sensor factory plug-ins. */
    MotionReaderXML(const std::map<std::string, boost::shared_ptr<MMM::SensorFactory> > &sensorFactories);

    /*! Load a specific motion from an mmm dataformat xml document.
        @param xml An xml string or a path to an xml document.
        @param motionName The name of the motion to load from the xml document.
        @param xmlIsPath Xml parameter is a path or string.
        @throws XMLFormatException When the mmm data format is not applicable.*/
    MotionPtr loadMotion(const std::string &xml, const std::string &motionName, bool xmlIsPath = true);

    /*! Load all motions from an mmm dataformat xml document.
        @param xml An xml string or a path to an xml document.
        @param xmlIsPath Xml parameter is a path or string.
        @throws XMLFormatException When the mmm data format is not applicable.*/
    MotionList loadAllMotions(const std::string &xml, bool xmlIsPath = true);

    static std::string getStandardLibPath();

private:
    MotionPtr loadMotion(RapidXMLReaderNodePtr motionNode, const std::string &filePath);

    std::map<std::string, boost::shared_ptr<MMM::SensorFactory> > sensorFactories;

    static const std::string ERROR_MESSAGE;
};

typedef boost::shared_ptr<MotionReaderXML> MotionReaderXMLPtr;
}
#endif // __MMM_MOTIONREADERXML_H_

