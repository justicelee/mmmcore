/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORMEASUREMENT_H_
#define __MMM_SENSORMEASUREMENT_H_

#include "../MMMCore.h"
#include "../MMMImportExport.h"


#include "../RapidXML/RapidXMLWriter.h"

namespace MMM
{

class SensorMeasurement;

typedef boost::shared_ptr<SensorMeasurement> SensorMeasurementPtr;

/*! \brief The public interface for the sensor measurement classes. */
class MMM_IMPORT_EXPORT SensorMeasurement
{
public:

    /*! Appends this sensor measurement as an xml representation to a given xml node.
        @param node The xml node.*/
    virtual void appendDataXML(RapidXMLWriterNodePtr node);

    /*! Returns a copy of this sensor measurement. */
    virtual SensorMeasurementPtr clone() = 0;

    /*! Checks if specific sensor measurement values are equal. (not checking timestep or interpolated) */
    virtual bool equals(SensorMeasurementPtr sensorMeasurement) = 0;

    /*! Returns an xml string representation of this sensor measurement. */
    std::string toXMLString();

    float getTimestep();

    bool isInterpolated();

protected:
    SensorMeasurement(float timestep, bool interpolated);

    virtual void appendMeasurementDataXML(RapidXMLWriterNodePtr measurementNode) = 0;

    /*! Timestep of the sensor measurement.*/
    float timestep;

    /*! Wheter this sensor measurement is interpolated or a measured value. */
    bool interpolated;
};

template <typename T>
class SMCloneable
{
public:
    virtual boost::shared_ptr<T> clone(float newTimestep) = 0;
};

}
#endif // __MMM_SENSORMEASUREMENT_H_

