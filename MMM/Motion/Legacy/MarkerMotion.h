/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
* The sources are based on the OpenMMM 2010 package by Stefan Gaertner and Martin Do.
*/

#ifndef __MMM_MarkerMotion_H_
#define __MMM_MarkerMotion_H_

#include "../../MMMCore.h"
#include <string>
#include <map>
#include <Eigen/Core>
#include <fstream>
#include <iostream>

#include "../../MMMImportExport.h"
#include "AbstractMotion.h"
#include "LegacyMarkerData.h"

namespace MMM
{

/*!
	\brief This calls defines a Cartesian motion of a set of markers.
*/
class MMM_IMPORT_EXPORT MarkerMotion : public AbstractMotion
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	MarkerMotion(unsigned int frames = 0);

	//! Number of entries
	virtual unsigned int getNumFrames();

	//! Number of markers
	size_t getNumMarkers();

	void setNumFrames(size_t frames);

	/*!
		Returns the name of label i.
	*/
	std::string getMarkerLabel(size_t i);

	//! Set the label name data
	void setMarkerLabels(std::vector< std::string > &labels);

	//! Returns the names of all markers.
	std::vector< std::string > getMarkerLabels();

	//! Checks if given label is present
	bool hasMarkerLabel(const std::string &labelName) const;

	//! Get entry at a specific frame.
	LegacyMarkerDataPtr getFrame(size_t frame);

	//! Fill data
	void setFrame(size_t frame, LegacyMarkerDataPtr data);

	//! Append marker data to end of data vector.
	void appendFrame(LegacyMarkerDataPtr data);

	/*!
		Creates an XML string for this object.
	*/
	virtual std::string toXML();


    //! get a segment motion from the original motion
    void segment(size_t frame1, size_t frame2);


	//! Print data.
	void print();

protected:

	std::vector< LegacyMarkerDataPtr > markerTrajectory;
	std::vector< std::string > markerLabels;
};

typedef boost::shared_ptr<MarkerMotion> MarkerMotionPtr;

}

#endif
