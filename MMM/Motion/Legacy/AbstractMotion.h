/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_AbstractMotion_H_
#define __MMM_AbstractMotion_H_

#include "../../MMMCore.h"

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

#include "../../MMMImportExport.h"
#include "MotionEntries.h"
#include "MotionFrame.h"

namespace MMM
{

/*!
	\brief An interface for all motion classes.
*/
class MMM_IMPORT_EXPORT AbstractMotion
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	AbstractMotion();

	/*!
		Returns the number of entries.
	*/
	virtual unsigned int getNumFrames() = 0;

	/*!
		Creates an XML string for this object.
	*/
	virtual std::string toXML() = 0;

	
	/*! 
        Adds a custom motion entry.  
        Silently overwrites existing entries with same name (lowercase).
        \param name The name is used to identify the custom data. Internally it is converted to lowercase.
        \param entry The data to store.
        \return True on success.
	*/
	virtual bool addEntry(const std::string &name, MotionEntryPtr entry);

	//! Remove custom entry 
	virtual bool removeEntry(const std::string &name);

	//! Check if entry with name (lowercase) is present.
	virtual bool hasEntry(const std::string &name) const;

	//! Retrieve custom motion data entries. Name is internally converted to lowercase.
	virtual MotionEntryPtr getEntry(const std::string &name);

protected:

	//! Custom Entries are stored in this map
	std::map<std::string, MotionEntryPtr> motionEntries;
	
};

typedef boost::shared_ptr<AbstractMotion> AbstractMotionPtr;

}

#endif 
