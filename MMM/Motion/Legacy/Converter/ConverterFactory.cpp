#include "ConverterFactory.h"
#include "../../../AbstractFactoryMethod.h"

namespace MMM
{

// Need to add a template specialization to the library, 
// otherwise we will get undefined references when we use the ConverterFactory externally.
template class MMM_IMPORT_EXPORT AbstractFactoryMethod<ConverterFactory, void*>;

}
