/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
* The sources are based on the OpenMMM 2010 package by Stefan Gaertner and Martin Do.
*/

#ifndef __MMM_LegacyMarkerData_H_
#define __MMM_LegacyMarkerData_H_

#include "../../MMMCore.h"
#include <string>
#include <map>
#include <Eigen/Core>
#include <fstream>
#include <iostream>

#include "../../MMMImportExport.h"


namespace MMM
{

/*!
	\brief A data entry of a MarkerMotion.
*/
class MMM_IMPORT_EXPORT LegacyMarkerData
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
			
	//! The frame
	int frame;

	//! Timestamp (s)
	float timestamp;

	//! marker position data
	std::map< std::string, Eigen::Vector3f > data;

	void print()
	{
		cout << "- frame:" << frame << endl;
		cout << "- timestamp:" << timestamp << endl;
		cout << "- marker poses:" << endl;
		std::map< std::string, Eigen::Vector3f >::iterator it = data.begin();
		while (it != data.end())
		{
			cout << "-- " << it->first << ":\t" << it->second(0) << "," << it->second(1) << "," << it->second(2) << endl;
			it++;
		}
	}

	std::string toXML()
	{
		std::string tab = "\t";
		std::stringstream res;

		res << tab << "<motion-data>" << endl;
		res << tab << tab << "<frame>" << frame << "</frame>" << endl;
		res << tab << tab << "<timestamp>" << timestamp << "</timestamp>" << endl;
		std::map< std::string, Eigen::Vector3f >::iterator it = data.begin();
		while (it != data.end())
		{
			res << tab << tab << "<marker_position>" << it->second(0) << " " << it->second(1) << " " << it->second(2)  << "</marker_position>" << endl;
			it++;
		}
		res << tab << "</motion-data>" << endl;
		return res.str();
	}
};
typedef boost::shared_ptr<LegacyMarkerData> LegacyMarkerDataPtr;

}

#endif
