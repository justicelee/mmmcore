/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*/

#ifndef __MMM_UnlabeledMarkerMotion_H_
#define __MMM_UnlabeledMarkerMotion_H_

#include "../../MMMCore.h"
#include <vector>

#include "../../MMMImportExport.h"
#include "AbstractMotion.h"
#include "UnlabeledMarkerData.h"

namespace MMM
{

class MMM_IMPORT_EXPORT UnlabeledMarkerMotion : public AbstractMotion
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    // Implement pure virtual methods from AbstractMotion
    virtual unsigned int getNumFrames();
    virtual std::string toXML();

    void appendFrame(UnlabeledMarkerDataPtr data);
    UnlabeledMarkerDataPtr getFrame(size_t frame) const;

protected:
    std::vector<UnlabeledMarkerDataPtr> markerTrajectories;
};

typedef boost::shared_ptr<UnlabeledMarkerMotion> UnlabeledMarkerMotionPtr;

}

#endif
