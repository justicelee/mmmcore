/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MotionEntries_H_
#define __MMM_MotionEntries_H_

#include "../../MMMCore.h"
#include "../../MMMImportExport.h"

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

namespace MMM
{
/*!
	\brief The interface for entires of the motion.
    Custom motion entries (motion XML tags in MMM motion files) have to derive from this class.
*/
class MMM_IMPORT_EXPORT MotionEntry
{
public:
	MotionEntry(const std::string &tagName)
        : tagName(tagName)
    {
    }

	//! Must provide an output method to generate top-level XML tags
	virtual std::string toXML() = 0;

    //! The name of the XML tag.
    std::string tagName;
};
typedef boost::shared_ptr<MotionEntry> MotionEntryPtr;

/*!
	A standard entry: comments
*/
class MMM_IMPORT_EXPORT CommentEntry : public MotionEntry
{
public:
	CommentEntry():MotionEntry("comments")
	{
	}

	//! Generate XML tag
	virtual std::string toXML()
	{
		std::string tab = "\t\t";
		std::string tab2 = "\t\t\t";
		std::stringstream res;
		res << tab << "<" << tagName << ">" << std::endl;
		for (size_t i=0;i<comments.size();i++)
		{
			res << tab2 << "<text>" << comments[i] << "</text>" << std::endl;
		}
		res << tab << "</" << tagName << ">" << endl;
		return res.str();
	}

	std::string getCommentString()
	{
		std::string c;
		for (size_t i=0;i<comments.size();i++)
		{
			c += comments[i];
			c += "\n";
		}
		return c;
	}

    //! The text comments
	std::vector< std::string > comments;
};
typedef boost::shared_ptr<CommentEntry> CommentEntryPtr;

}

#endif 
