/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*/

#ifndef __MMM_UnlabeledMarkerData_H_
#define __MMM_UnlabeledMarkerData_H_

#include "../../MMMCore.h"
#include "../../MMMImportExport.h"

#include <Eigen/Core>
#include <vector>

namespace MMM
{

/*!
    \brief A data entry of an UnlabeledMarkerMotion.
*/
class MMM_IMPORT_EXPORT UnlabeledMarkerData
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
			
	//! The frame
	int frame;

	//! Timestamp (s)
	float timestamp;

	//! marker position data
    std::vector<Eigen::Vector3f> data;
};

typedef boost::shared_ptr<UnlabeledMarkerData> UnlabeledMarkerDataPtr;

}

#endif
