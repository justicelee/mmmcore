#include "AbstractMotion.h"
#include "../../RapidXML/rapidxml.hpp"
#include "../../XMLTools.h"

using std::cout;
using std::endl;

namespace MMM
{

AbstractMotion::AbstractMotion()
{

}


bool AbstractMotion::addEntry( const std::string &name, MotionEntryPtr entry )
{
	std::string lc = name;
	XML::toLowerCase(lc);
	motionEntries[lc] = entry;
	return true;
}

MMM::MotionEntryPtr AbstractMotion::getEntry( const std::string &name )
{
	std::string lc = name;
	XML::toLowerCase(lc);
	if (!hasEntry(lc))
	{
		MMM_WARNING << "Could not find entry with name:" << name << endl;
		return MotionEntryPtr();
	}
	return motionEntries[lc];
}

bool AbstractMotion::hasEntry( const std::string &name ) const
{
	std::string lc = name;
	XML::toLowerCase(lc);
	if (motionEntries.find(lc) != motionEntries.end())
	{
		return true;
	}
	return false;
}

bool AbstractMotion::removeEntry( const std::string &name )
{
	std::string lc = name;
	XML::toLowerCase(lc);
	if (hasEntry(lc))
		motionEntries.erase(lc);
	return true;
}


}
