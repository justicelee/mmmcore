
#include "LegacyMotionReaderC3D.h"
#include <string>
#include <iostream>

#define DETAILED_INFORMATION

using namespace std;

namespace MMM
{


LegacyMotionReaderC3D::LegacyMotionReaderC3D() {
	last_field=0;
	first_field=0;
	num_used_markers = 0;
	video_rate = 120;
}

LegacyMotionReaderC3D::~LegacyMotionReaderC3D() {
	//nothing to be done here
}

float LegacyMotionReaderC3D::convertFloat(char mem_float[4]) {
	switch ((int)ptype) {
	case 84:
		return *(float*) mem_float;
	case 85:
		return ConvertDecToFloat(mem_float);
	case 86:
		MMM_WARNING << "MIPS is not supported yet." <<endl;
		return -1;
	default:
		MMM_WARNING << "unknown processor type." << endl;
		return -1;
	}
}

//Utilities to convert floating points to and from the DEC file format used by Vicon.
//Records are 256 (16 bit) words long.
float LegacyMotionReaderC3D::ConvertDecToFloat(char bytes[4]) {
	char p[4];
	p[0] = bytes[2];
	p[1] = bytes[3];
	p[2] = bytes[0];
	p[3] = bytes[1];
	if (p[0] || p[1] || p[2] || p[3]) {
		--p[3]; // adjust exponent
	}

	return *(float*) p;
}

void LegacyMotionReaderC3D::ConvertFloatToDec(float f, char* bytes) {
	char* p = (char*) &f;
	bytes[0] = p[2];
	bytes[1] = p[3];
	bytes[2] = p[0];
	bytes[3] = p[1];
	if (bytes[0] || bytes[1] || bytes[2] || bytes[3]) {
		++bytes[1]; // adjust exponent
	}
}

long LegacyMotionReaderC3D::getFilePositionGroup(const char* group) {

	long gbyte;
	char gname[25];

	// Start at Byte 512 of infile
	// First four bytes specified
	infile.seekg(512+4, ios::beg);

#ifdef DEBUG
		cout << "searching for group "<< group <<" ... ";
#endif
	while (strncmp(gname, group, strlen(group)) != 0 && !infile.eof()) {
		infile.read(reinterpret_cast<char *>(&gname), sizeof(char)*strlen(group));
		infile.seekg(-1*static_cast<int>((strlen(group)-1)*sizeof(char)),ios::cur);
	}
	if (!infile.eof()) {
		infile.seekg((strlen(group)-1)*sizeof(char),ios::cur);

		// Record this file position to go back to for each parameter in group
		gbyte = (long)infile.tellg();

#ifdef DEBUG
		cout << "found group << " << group << endl;
#endif

	} else {
		gbyte = -1;
	}

	return gbyte;

}

long LegacyMotionReaderC3D::getFilePositionParams(long group_pos, const char* parameters) {

	char pname[25];
	long pbyte;

	infile.seekg(group_pos, ios::beg);

	// Scan for each parameter of interest in group POINT
	while (strncmp(pname,parameters,strlen(parameters)) != 0 && !infile.eof())
	{
		infile.read(reinterpret_cast<char *>(&pname),sizeof(char)*strlen(parameters));
		infile.seekg(-1*static_cast<int>((strlen(parameters)-1)*sizeof(char)),ios::cur);
	}

	if (!infile.eof()) {
		// reposition to end of LABELS
		infile.seekg((strlen(parameters)-1)*sizeof(char),ios::cur);

		// Record this file position
		pbyte = (long)infile.tellg();

#ifdef DEBUG
	cout << "\tfound parameter " << parameters << endl;
#endif

	} else {
		pbyte = -1;
	}
	return pbyte;
}

// Reads in parameter records from C3D file passed
// All data in header is specified as "16 bit words", or the equivalent of one unsigned char
void LegacyMotionReaderC3D::Read_C3D_Marker_Labels() {

	char dim, type;
	unsigned char nrow, ncol;
	unsigned int i, j;
	int offset;
	long gbyte, pbyte;

	char **prefixes;
	unsigned int num_prefixes = 0;
	signed short is_prefixes = 0;

	if (infile.is_open() ) {

		// Because it is unknown how many groups and how many
		// parameters/group are stored, must scan for each variable
		// of interest.

		// 1st scan for group SUBJECT
		// Parameters stored in SUBJECTS are:
		//		1. USES_PREFIXES
		//		2. LABEL_PREFIXES

		gbyte = getFilePositionGroup("SUBJECT");

		if (gbyte > 0) {

			pbyte = getFilePositionParams(gbyte, "USES_PREFIXES");

			if (pbyte > 0) {

				infile.seekg(pbyte,ios::beg);
				RPF(&offset, &type, &dim);

				infile.read(reinterpret_cast<char *>(&is_prefixes), sizeof(is_prefixes)*1);

				if (is_prefixes == 1) {
					pbyte = getFilePositionParams(gbyte, "LABEL_PREFIXES");

					if (pbyte > 0) {
						infile.seekg(pbyte,ios::beg);
						RPF(&offset, &type, &dim);
						infile.read(reinterpret_cast<char *>(&ncol), sizeof(char)*1);
						infile.read(reinterpret_cast<char *>(&nrow), sizeof(char)*1);

						num_prefixes = nrow;
						prefixes = new char*[nrow];
						for (i=1;i<=num_prefixes;i++)
						{
							prefixes[i-1] = new char[ncol];
							for (j=1;j<=ncol;j++)
							{
								infile.read(&prefixes[i-1][j-1],sizeof(char)*1);
								if (prefixes[i-1][j-1] == ' ') {
									prefixes[i-1][j-1] = 0;
								}
							}
						}

					}
				}
			}

		}

		// 2nd scan for group POINT
		// Parameters stored in POINT are:
		//		1. LABELS
		//		2. DESCRIPTIONS
		//		3. USED
		//		4. UNITS
		//		5. SCALE
		//		6. RATE
		//		7. DATA_START
		//		8. FAMES
		//		9. INITIAL_COMMAND
		//		10.X_SCREEN
		//		11.Y_SCREEN
		labels.resize(num_markers);

		gbyte = getFilePositionGroup("POINT");

		if (gbyte > 0) {

			pbyte = getFilePositionParams(gbyte, "LABELS");

			if (pbyte > 0) {

				infile.seekg(pbyte,ios::beg);

				RPF(&offset, &type, &dim); // dim should be 2 for a 2D array of labels[np][4]
				// read in array dimensions: should be 4 x np
				infile.read(reinterpret_cast<char *>(&ncol), sizeof(char)*1);
				infile.read(reinterpret_cast<char *>(&nrow), sizeof(char)*1);

				for (i=1;i<=nrow;i++)
				{
					//lables[i-1] = new char[ncol];
					char *label = new char[ncol];
					for (j=1;j<=ncol;j++)
					{
						infile.read(reinterpret_cast<char *>(&label[j-1]),sizeof(char)*1);
						if (label[j-1] == ' ') {
							label[j-1] = 0;
						}
					}
					labels[i-1] = label;
					delete []label;
				}
			}

			if (num_markers > 255) {

				pbyte = getFilePositionParams(gbyte, "LABELS2");

				if (pbyte > 0) {
					infile.seekg(pbyte,ios::beg);

					RPF(&offset, &type, &dim); // dim should be 2 for a 2D array of labels[np][4]
					// read in array dimensions: should be 4 x np
					infile.read(reinterpret_cast<char *>(&ncol), sizeof(char)*1);
					infile.read(reinterpret_cast<char *>(&nrow), sizeof(char)*1);

					for (i=1;i<=nrow;i++)
					{
						//mlabels[i-1+255] = new char[ncol];
						char *label = new char[ncol];
						for (j=1;j<=ncol;j++)
						{
							infile.read(reinterpret_cast<char *>(&label[j-1]),sizeof(char)*1);
							if (label[j-1] == ' ') {
								label[j-1] = 0;
							}
						}
						labels[i-1+255] = label;
						delete []label;
					}
				}
			}
		}
	}
}

void LegacyMotionReaderC3D::RPF(int *offset, char *type, char *dim) {
	char offset_low, offset_high;
	// Read Parameter Format for the variable following the
	// parameter name
	//		offset = number of bytes to start of next parameter (2 Bytes, reversed order: low, high)
	//		T = parameter type: -1 = char, 1 = boolean, 2 = int, 3 = float
	//		D = total size of array storage (incorrect, ignore)
	//		d = number of dimensions for an array parameter (d=0 for single value)
	//		dlen = length of data
	infile.read(&offset_low, sizeof(char)*1); // byte 1
	infile.read(&offset_high, sizeof(char)*1); // byte 2
	*offset = 256* offset_high + offset_low;
	infile.read(type, sizeof(char)*1); // byte 3
	infile.read(dim, sizeof(char)*1); // byte 4
}

//Reads in header information from C3D file passed
void LegacyMotionReaderC3D::Read_C3D_Header() {

	unsigned short key1, max_gap;
	char cdum;
	char mem_float[4];

	if (infile.is_open()) {

		// Read processor type
		infile.seekg(512, ios::beg);

		// First four bytes specified
		// bytes 1 and 2 are part of ID key (1 and 80)
		infile.read(&cdum, sizeof(char)* 1); // byte 1
		infile.read(&cdum, sizeof(char)*1); // byte 2
		// byte 3 holds # of parameter records to follow
		infile.read(&cdum, sizeof(char)*1); // byte 3
		// byte 4 is processor type, Vicon uses DEC (type 2)
		infile.read(reinterpret_cast<char *>(&ptype), sizeof(char)*1); // byte 3

		/* Read in Header */
		infile.seekg(0, ios::beg);
		// Key1, byte = 1,2; word = 1
		infile.read(reinterpret_cast<char *>(&key1), sizeof(key1)*1);

		// Number of 3D points per field, byte = 3,4; word = 2
		infile.read(reinterpret_cast<char *>(&num_markers), sizeof(num_markers) *1);
        num_used_markers = num_markers;

        // Number of analog channels per field byte = 5,6; word = 3
		infile.read(reinterpret_cast<char *>(&num_channels), sizeof(num_channels)*1);

		// Field number of first field of video data, byte = 7,8; word = 4
		infile.read(reinterpret_cast<char *>(&first_field), sizeof(first_field)*1);

		// Field number of last field of video data, byte = 9,10; word = 5
		infile.read(reinterpret_cast<char *>(&last_field), sizeof(last_field)*1);

		// Maximum interpolation gap in fields, byte = 11,12; word = 6
		infile.read(reinterpret_cast<char *>(&max_gap), sizeof(max_gap)*1);

		// Scaling Factor, bytes = 13,14,15,16; word = 7,8
		infile.read(&mem_float[0], sizeof(char)*1); // 1st byte
		infile.read(&mem_float[1], sizeof(char)*1); // 2nd byte
		infile.read(&mem_float[2], sizeof(char)*1); // 3rd byte
		infile.read(&mem_float[3], sizeof(char)*1); // 4th byte

		scale_factor = convertFloat(mem_float);

		// Starting record number, byte = 17,18; word = 9
		infile.read(reinterpret_cast<char *>(&start_record_num), sizeof(start_record_num)*1);

		// Number of analog frames per video field, byte = 19,20; word = 10
		infile.read(reinterpret_cast<char *>(&frames_per_field), sizeof(frames_per_field)*1);

		// Analog channels sampled
		if (frames_per_field != 0)
			num_channels /= frames_per_field;

		// Video rate in Hz, bytes = 21,22,23,24; word = 11,12
		infile.read(&mem_float[0], sizeof(char)*1); // 1st byte
		infile.read(&mem_float[1], sizeof(char)*1); // 2nd byte
		infile.read(&mem_float[2], sizeof(char)*1); // 3rd byte
		infile.read(&mem_float[3], sizeof(char)*1); // 4th byte

		video_rate =  convertFloat(mem_float);

#ifdef DEBUG
		// debug output
		cout << "Number of Markers:" << num_markers <<endl;
		cout << "Number of Channels: " << num_channels << endl;
		cout << "First field: " << first_field << endl;
		cout << "Last Field: " << last_field << endl;
		cout << "Scale Factor: " << scale_factor << endl;
		cout << "Start Record Number: " << start_record_num << endl;
		cout << "Frames per Field: " << frames_per_field << endl;
		cout << "Video Rate: " << video_rate << endl;
		cout << "Processor Type: ";
		switch ((int)ptype) {
		case 84:
			cout << "Intel" << endl;
			break;
		case 85:
			cout << "DEC" << endl;
			break;
		case 86:
			cout << "MIPS (not supported)" << endl;
			break;
		default:
			cout << "unknown" << endl;
		}
#endif
	}
}

// Reads in 3D position data from C3D file passed
// All data in header is specified as "16 bit words", or the equivalent of one unsigned char
void LegacyMotionReaderC3D::Read_C3D_Marker(unsigned short marker_num, // Read this marker #
								std::vector< Eigen::Vector3f > &val, 
								Eigen::VectorXf &res)
{
	unsigned short frame_length, frame_num, i, offset;
	char cam;
	res.resize(getNumFrames());

	if (infile.is_open()) {
		// Data is stored in the following format
		// Each frame (or field) from first_field to last_field
		//		Each marker from 1 to num_markers
		//			Data: X,Y,Z,R,C (Total of 8 bytes)
		//		Next marker
		//		Each analog sample per frame from 1 to analog_frames_per_field
		//			Each analog channel from 1 to num_analog_channels
		//				Data: A (total of 2 bytes)
		//			Next channel
		//		Next sample
		// Next frame

		char data_length;
		char analog_bytes;

		if (scale_factor < 0.0) {
			data_length = 16;
			analog_bytes = 4;
		} else {
			data_length = 8;
			analog_bytes = 2;
		}

		// Startbyte is the starting location of tvd/adc data
		long start_byte = 512 * (start_record_num -1);

		// Determine data offset based upon starting channel number
		offset = data_length* (marker_num -1);

		// Determine interval to next frame of this markers data
		frame_length = (data_length*(num_markers-1))+(analog_bytes*num_channels*frames_per_field);

		// Position cursor to first data point

		infile.seekg(start_byte+offset,ios::beg);
		char mem_float[4];

		for (frame_num = first_field; frame_num <= last_field; frame_num++)
		{
			int index = frame_num-first_field;
			// Read XYZ data for this frame
			for (i=1;i<=3;i++) // for x,y,z
			{
				if (scale_factor < 0.0) {
					infile.read(&mem_float[0], sizeof(char)*1); // 1st byte
					infile.read(&mem_float[1], sizeof(char)*1); // 2nd byte
					infile.read(&mem_float[2], sizeof(char)*1); // 3rd byte
					infile.read(&mem_float[3], sizeof(char)*1); // 4th byte
					val[index][i-1] = convertFloat(mem_float);

				} else {
					short pos;
					infile.read(reinterpret_cast<char *>(&pos), sizeof(pos)* 1);
					val[index][i-1] = pos*scale_factor;
				}
			}

			if (scale_factor < 0.0) {
				infile.read(&mem_float[0], sizeof(char)*1); // 1st byte
				infile.read(&mem_float[1], sizeof(char)*1); // 2nd byte
				infile.read(&mem_float[2], sizeof(char)*1); // 3rd byte
				infile.read(&mem_float[3], sizeof(char)*1); // 4th byte

				//TODO convert data in order to get residual and # cameras

			} else {
				// Read Residual
				char c;
				infile.read(&c, sizeof(char)* 1);
				res[index] = c;
				// Read # cameras
				infile.read(&cam, sizeof(char)* 1);
			}
			// Skip to the next frame
			infile.seekg(frame_length,ios::cur);
		}

#ifdef DEBUG
		cout << endl << "Marker Data" << endl;
		for (int j=0; j<5;j++) {
			cout << "frame: " << j+first_field << endl;
			cout << " x: " << val[j][0] << endl;
			cout << " y: " << val[j][1] << endl;
			cout << " z: " << val[j][2] << endl;
		}
#endif
	}
}

bool LegacyMotionReaderC3D::validate()
{
	if (!infile.is_open())
		return false;
	// File exists and is open
	char cFlag, cPointer, cNumBlocks, cFormat;
	unsigned int iPosition;

	infile.seekg(0, ios::beg);

	// First two bytes are essential for validation
	// bytes 1 and 2 are part of ID key (1 and 80)
	infile.read(&cPointer, sizeof(char)*1); // byte 1
	infile.read(&cFlag, sizeof(char)*1); // byte 2
	if (cFlag != 0x50)
	{
#ifdef DETAILED_INFORMATION
		MMM_WARNING << "cFlag was not correct! Value [" << (int)cFlag << "]!=0x50" << endl;
#endif
		return false;
	}
#ifdef DETAILED_INFORMATION
		cout << "cFlag was correct [value == 0x50]!" << endl;
#endif
	// first check was successful
	iPosition = 256*cPointer+2; // on c3d.org this factor is said to be 512, but works only with 256
	// iPosition points at the start of the parameter records
	// now ignore the first word and read the second
	infile.seekg(iPosition, ios::beg);
	infile.read(&cNumBlocks, sizeof(char)*1); // byte 3
	infile.read(&cFormat, sizeof(char)*1); // byte 4
	// Format should be in range of [0x54..0x56]
	if ((cFormat<0x54) | (cFormat>0x56))
	{
#ifdef DETAILED_INFORMATION
		MMM_WARNING << "cFormat was not correct! Value [" << (int)cFormat << "] is not in range of [0x54..0x56]!" << endl;
#endif
		return false;
	}
#ifdef DETAILED_INFORMATION
	cout << "cFormat was correct! Value [" << (int)cFormat << "]  is in range of [0x54..0x56]!" << endl;
	cout << "This is probably a correct C3D File! Number of 512byte Blocks in the parameter section: "<< (int)cNumBlocks << "." << endl;
#endif
	return true;
}


MarkerMotionPtr LegacyMotionReaderC3D::loadC3D(const std::string filename) {
	this->filename = filename;
	labels.clear();
	last_field = 0;
	first_field = 0;
	num_used_markers = 0;
	video_rate = 120;
	infile.open((char*)filename.c_str(), ios::in | ios::binary);
	Read_C3D_Header();

    Read_C3D_Marker_Labels();
	if (labels.size()==0)
	{
        MMM_ERROR << "Error no label data in file (Please check the spelling of prefix if you have set it.)" << filename << endl;
		return MarkerMotionPtr();
	}
	MarkerMotionPtr result(new MarkerMotion());
	result->setMarkerLabels(labels);

	readMarkerData(result);

	return result;
}

UnlabeledMarkerMotionPtr LegacyMotionReaderC3D::loadUnlabeledC3D(const std::string filename) {
    this->filename = filename;
    labels.clear();
    last_field = 0;
    first_field = 0;
    num_used_markers = 0;
    video_rate = 120;
    infile.open((char*)filename.c_str(), ios::in | ios::binary);
    Read_C3D_Header();

    std::vector<std::vector<Eigen::Vector3f> > allMarkerTrajectories;
    int numFrames = getNumFrames();

    for (int curMarker = 1; curMarker <= num_used_markers; ++curMarker)
    {
        std::vector<Eigen::Vector3f> trajectory;
        Eigen::VectorXf res;

        trajectory.resize(numFrames);

        Read_C3D_Marker(curMarker, trajectory, res);

        allMarkerTrajectories.push_back(trajectory);
    }

    UnlabeledMarkerMotionPtr motion(new UnlabeledMarkerMotion());

    for (int curFrame = 0; curFrame < numFrames; ++curFrame)
    {
        UnlabeledMarkerDataPtr markerData(new UnlabeledMarkerData());

        markerData->frame = curFrame;
        markerData->timestamp = (float)curFrame / video_rate;

        for (std::vector<std::vector<Eigen::Vector3f> >::const_iterator it = allMarkerTrajectories.begin(); it != allMarkerTrajectories.end(); ++it)
        {
            Eigen::Vector3f markerPosition = (*it)[curFrame];

            // Since we do not read the residual values right now, we use the position to check if the marker point is valid
            // (and hope that there will not be a valid marker exactly at the origin
            if (!markerPosition.isZero())
                markerData->data.push_back(markerPosition);
        }

        motion->appendFrame(markerData);
    }

    return motion;
}

void LegacyMotionReaderC3D::readMarkerData(unsigned short marker_num, std::vector<Eigen::Vector3f> &val, Eigen::VectorXf &res){//float **val, unsigned char *res) {
	if (marker_num < num_used_markers) {
		Read_C3D_Marker(marker_num+1,val,res);
	} else
	{
		MMM_ERROR << "readMarkerData wrong pos:" << marker_num << endl;
	}
}

void LegacyMotionReaderC3D::readMarkerData(MarkerMotionPtr data)
{
	if (!data || getNumFrames()==0)
		return;
	MMM_INFO << "Number Frames: " << getNumFrames() << endl;

	data->setNumFrames(getNumFrames());
	std::vector< Eigen::Vector3f > val(getNumFrames());
	int frame;
	for (frame = 0; frame < getNumFrames(); frame++) {
		val[frame].setZero();
	}	
	Eigen::VectorXf res(getNumFrames());
    for (int marker = 0; marker < num_used_markers; marker++)
	{
		readMarkerData(marker,val,res);
		string marker_name = labels[marker];
        string prefix = marker_name.substr(0, marker_name.find(":"));

        if(!markerPrefix.empty() && prefix != markerPrefix) continue;

        marker_name = marker_name.substr(marker_name.find(":")+1);

		if (marker_name.empty())
		{
			cout << "Empty marker name..." << endl;
		}
		else
		{
			for (frame = 0; frame < getNumFrames(); frame++) {
				// update entry
				LegacyMarkerDataPtr md = data->getFrame(frame);
				md->data[marker_name] = val[frame];
				md->frame = frame;
				md->timestamp = (float)frame / video_rate;
			}
		}
	}
}


int LegacyMotionReaderC3D::getNumMarkers(){
	return num_used_markers;
}

int LegacyMotionReaderC3D::getNumFrames() {
	return last_field - first_field+1;
}


}

