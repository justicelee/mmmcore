#include "Sensor.h"

using namespace MMM;


Sensor::Sensor(const std::string &description) :
    description(description)
{
}

std::string Sensor::getName() {
    return name;
}

void Sensor::setName(const std::string &name) {
    this->name = name;
}

std::string Sensor::getUniqueName() {
    return uniqueName;
}

void Sensor::setUniqueName(const std::string &uniqueName) {
    this->uniqueName = uniqueName;
}

std::string Sensor::getDescription() {
    return description;
}

void Sensor::setDescription(const std::string &description) {
    this->description = description;
}

bool Sensor::isInterpolatable() {
    return false;
}

int Sensor::getPriority() {
    return 0;
}

SensorPtr Sensor::join(SensorPtr sensor1, SensorPtr sensor2) {
    return sensor1->joinSensor(sensor2);
}

void Sensor::synchronizeSensorMeasurements(float timeFrequency) {
    synchronizeSensorMeasurements(timeFrequency, getMinTimestep(), getMaxTimestep());
}

void Sensor::synchronizeSensorMeasurements(float timeFrequency, float minTimestep, float maxTimestep) {
    // do nothing!
}

