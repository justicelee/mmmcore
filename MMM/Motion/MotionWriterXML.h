/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONWRITERXML_H_
#define __MMM_MOTIONWRITERXML_H_

#include "../MMMCore.h"
#include "../MMMImportExport.h"
#include "Motion.h"

#include <string>

namespace MMM
{
/*! \brief A generic xml writer for motions in the mmm dataformat */
class MMM_IMPORT_EXPORT MotionWriterXML
{
public:

    /*! Write a motion to an mmm dataformat (xml) string. The path can be stated to make a relative model path.*/
    static std::string toXMLString(MotionPtr motion, const std::string &path =  std::string());

    /*! Write multiple motions in one mmm dataformat (xml) string. The path can be stated to make a relative model path.*/
    static std::string toXMLString(MotionList motion, const std::string &path = std::string());

    /*! Write a motion to an xml document at the given path. */
    static void writeMotion(MotionPtr motion, const std::string &path);

    /*! Write multiple motions in one xml document at the given path. */
    static void writeMotion(MotionList motion, const std::string &path);

    /*! Write multiple motions in one xml document at the given path. If occured, a motion with the same name as the given motion will be replaced by it. */
    static void replaceAndWriteMotions(MMM::MotionPtr motion, MMM::MotionList motions, const std::string &motionFilePath);

private:

    static void appendMotionNode(RapidXMLWriterNodePtr root, MotionPtr motion, const std::string &path);
};

typedef boost::shared_ptr<MotionWriterXML> MotionWriterXMLPtr;
}
#endif // __MMM_MOTIONWRITERXML_H_


