#include "../XMLTools.h"

#include "SensorMeasurement.h"

using namespace MMM;

SensorMeasurement::SensorMeasurement(float timestep, bool interpolated):
    timestep(timestep),
    interpolated(interpolated)
{
}

void SensorMeasurement::appendDataXML(RapidXMLWriterNodePtr node) {
    assert(node);

    RapidXMLWriterNodePtr measurementNode = node->append_node(XML::NODE_MEASUREMENT)->append_attribute(XML::ATTRIBUTE_TIMESTEP, XML::toString(timestep));
    if (interpolated) measurementNode->append_attribute(XML::ATTRIBUTE_INTERPOLATED, "true");
    appendMeasurementDataXML(measurementNode);
}

std::string SensorMeasurement::toXMLString() {
    RapidXMLWriterPtr writer(new RapidXMLWriter());
    RapidXMLWriterNodePtr measurementNode = writer->createRootNode(XML::NODE_MEASUREMENT)->append_attribute(XML::ATTRIBUTE_TIMESTEP, XML::toString(timestep));
    if (interpolated) measurementNode->append_attribute(XML::ATTRIBUTE_INTERPOLATED, "true");
    appendMeasurementDataXML(measurementNode);
    return writer->print(true);
}

float SensorMeasurement::getTimestep() {
    return timestep;
}

bool SensorMeasurement::isInterpolated() {
    return interpolated;
}

