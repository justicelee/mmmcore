#ifndef TESTHELPER_H
#define TESTHELPER_H

#include "MMM/Model/Model.h"
#include <set>

struct TestHelper {
    static MMM::ModelPtr createMockupModel(std::set<std::string> nodeNames) {
        MMM::ModelPtr model(new MMM::Model());
        for (auto const &nodeName : nodeNames) {
            MMM::ModelNodePtr node(new MMM::ModelNode());
            node->name = nodeName;
            model->addModelNode(node);
        }
        return model;
    }
};

#endif // TESTHELPER_H
