
#define BOOST_TEST_MODULE MMM_BOOST_TEST

#include "MMM/Motion/MotionReaderC3D.h"
#include "MMM/Motion/Legacy/LegacyMotionReaderC3D.h"
#include "MMM/Motion/Legacy/MarkerMotion.h"

#include <boost/test/unit_test.hpp>

#include <string>
#include <iostream>

struct MyConfig {
    MyConfig()
    {
        mr = boost::shared_ptr<MMM::MotionReaderC3D>(new MMM::MotionReaderC3D());
    }
    ~MyConfig()  {
        //teardown
    }

    boost::shared_ptr<MMM::MotionReaderC3D> mr;
};

#if  BOOST_VERSION  < 106200
    BOOST_GLOBAL_FIXTURE(MyConfig)
#else
    BOOST_GLOBAL_FIXTURE(MyConfig);
#endif

BOOST_AUTO_TEST_CASE( test )
{
    MyConfig myConfig;

    /*for (auto marker : myConfig.mr->loadC3D("/common/homes/students/meixner/MMMTools/data/WalkingStraightForward07.c3d")) {
    }
    for (auto marker : myConfig.mr->loadC3D("/common/homes/students/meixner/Downloads/S2KF_Unlabed_C3D/Arme_hinter_Ruecken_02.c3d")) {
            marker.second->print();
    }*/

    /*boost::shared_ptr<MMM::LegacyMotionReaderC3D> ml(new MMM::LegacyMotionReaderC3D());
    MMM::MarkerMotionPtr m = ml->loadC3D("/common/homes/students/meixner/MMMTools/data/WalkingStraightForward07.c3d");
    m->print();*/

}



// BOOST_AUTO_TEST_SUITE_END()
