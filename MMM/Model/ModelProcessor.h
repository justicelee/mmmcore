/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ModelProcessor_H_
#define __MMM_ModelProcessor_H_

#include "../MMMCore.h"
#include "../MMMImportExport.h"
#include "../Motion/Legacy/AbstractMotion.h"
#include "Model.h"
#include "../RapidXML/RapidXMLWriter.h"

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

// using forward declarations here, so that the rapidXML header does not have to be parsed when this file is included
namespace rapidxml
{
    template<class Ch>
    class xml_node;
}

namespace MMM
{


/*!
	\brief A processor to adapt a generic model to user specific demands.
*/
class MMM_IMPORT_EXPORT ModelProcessor
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		ModelProcessor();
	
	/*!
		Convert a model
	*/
	virtual ModelPtr convertModel(ModelPtr input) = 0;

	virtual bool setupFile(const std::string &filename);
	virtual bool setupXML(const std::string &xmlString);

	/*!
		Get model processor configuration as XML string.
	*/
	virtual std::string toXML(int nrTabs=0);

	virtual std::string getName();

    void appendDataXML(RapidXMLWriterNodePtr node) {
        RapidXMLWriterNodePtr processorNode = node->append_node("ModelProcessorConfig");
        if (!name.empty())
        {
            processorNode->append_attribute("type", name);
            appendProcessorDataXML(processorNode);
        }
    }

protected:
    //! first tag in xml configuration
    virtual bool _setup(rapidxml::xml_node<char>* rootTag) = 0;

    virtual void appendProcessorDataXML(RapidXMLWriterNodePtr /*processorNode*/) {
    }

	std::string name; //! name of modelprocessor
};

typedef boost::shared_ptr<ModelProcessor> ModelProcessorPtr;

}

#endif 
