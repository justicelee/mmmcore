# Changelog
===========================
All notable changes after version 2.0 in the %MMM framework will be documented in this file.

Version 2.1
---

**MMMCore**
* Added version checks to plugins
* Added MMM::MultipleFactoryPluginLoader to load multiple plugins with the same name identifier

**MMMTools**
* Added data glove to mmm data format converter

**MMMViewer**
* Activate/deactivate Plugins at Runtime
* Store session parameters like open motion file name, camera angle, options, ...
* Removed command line arguments
* Caching motion as temporary file to prevent work loss by crash



Version 2.0
---

For the perception, visualization, reproduction and recognition of multi-modal motion data, the %MMM framework and the %MMM dataformat was completely rebuild.

**MMMCore**
* New %MMM dataformat with matching motion reader and writer
* Using specific plugins for the new sensor measurement data
* New Methods for segmenting and joining motions
* New methods for synchronizing sensor data and sensors
* New Methods for accessing sensor data via interpolation or by a timestep delta.
* Template class for importing plugins
* Improved c3d motion reader
* Improved XML reader
* Legacy motion classes renamed and moved to legacy motion folder

Important name/folder changes (**includes and class names/pointer need to be adapted**)
* MMM/Converter -> MMM/Motion/Legacy/Converter/Converter
* MMM/ConverterFactory -> MMM/Motion/Legacy/Converter/ConverterFactory
* MMM/MarkerBasedConverter -> MMM/Motion/Legacy/Converter/MarkerBasedConverter
* MMM/rapidxml.hpp -> MMM/RapidXML/rapidxml.hpp
* MMM/rapidxml_print.hpp -> MMM/RapidXML/rapidxml_print.hpp
* MMM/Motion/AbstractMotion -> MMM/Motion/Legacy/AbstractMotion
* MMM/Motion/MarkerData -> MMM/Motion/Legacy/LegacyMarkerData
* MMM/Motion/MarkerMotion -> MMM/Motion/Legacy/MarkerMotion
* MMM/Motion/Motion -> MMM/Motion/Legacy/LegacyMotion
* MMM/Motion/MotionEntries -> MMM/Motion/Legacy/MotionEntries
* MMM/Motion/MotionFrame -> MMM/Motion/Legacy/MotionFrame
* MMM/Motion/MotionFrameEntries -> MMM/Motion/Legacy/MotionFrameEntries
* MMM/Motion/MotionReaderC3D -> MMM/Motion/Legacy/LegacyMotionReaderC3D
* MMM/Motion/MotionReaderXML -> MMM/Motion/Legacy/LegacyMotionReaderXML
* MMM/Motion/UnlabeledMarkerData -> MMM/Motion/Legacy/UnlabeledMarkerData
* MMM/Motion/UnlabeledMarkerMotion -> MMM/Motion/Legacy/UnlabeledMarkerMotion


**MMMTools**
* Tools for the legacy motion are not included by default and some of them were also renamed to 'Legacy...'
* New graphical user interface using various different plugins for visualization, reproduction and recognition
* Now includes motion segmentation and imu data convertation to the %MMM format

