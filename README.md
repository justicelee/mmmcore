Master Motor Map (MMM) Core
===========================

Description
---

Master Motor Map (MMM) is a conceptual framework for perception, visualization, reproduction, and recognition of human motion in order to decouple 
motion capture data from further post-processing tasks, such as execution on a real humanoid robot. Employing MMM makes it easy to map motions 
between different kinematics independently and uniformly as well as to analyze certain dynamic aspects of the considered motion.

Part of this framework is a dynamic model consisting of a particular kinematic structure enriched with pre-defined segment properties (anthropometric data) 
e.g. mass distribution, segment length, moment of inertia, etc. Moreover, the strategy is to define the maximum number of DoFs that might be used by 
any visualization, recognition, or reproduction module.

**This package (MMMCore) contains data structures and kinematic models (e.g. Data I/O, MMM Model, C3D Import) which are required for MMMTools.**


Obtaining MMM
---

The MMM framework is available for download on GitLab:

* [MMMCore](https://gitlab.com/mastermotormap/mmmcore): Data structures and kinematic models (e.g. Data I/O, MMM Model, C3D Import)
* [MMMTools](https://gitlab.com/mastermotormap/mmmtools): Tools for visualization, reproduction and recognition (e.g. the MMMViewer)


Installation & Documentation
---

For full documentation of the MMM project and installation instructions please refer to the [MMM Documentation](http://mmm.humanoids.kit.edu/).


Contributing to MMMCore
---

Information about contributing to MMM Core can be found in CONTRIBUTING.md.


License
---

MMMCore is released under the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.


Contact
---

Please refer to the webpage of the [High Performance Humanoid Technologies Lab](http://h2t.anthropomatik.kit.edu/english/).
